# Docker Image Builder for an STM32 Compiler

## Introduction

This repository contains everything needed to create a docker image which can then be used as a repeatable system/environment for building executables for the STM32 MCU.

The script which describes the docker image is `./docker/Dockerfile`.

The included `.gitlab-ci.yml` file describes a GitLab pipeline which (when triggered on GitLab) will automatically build the docker image described in `./docker/Dockerfile`. This pipeline will automatically build the docker image and then push the image to the Container Image Registry associated with this GitLab repository.

Once the docker image has been pushed to the Container Image Registry on GitLab, it can be used either by other GitLab pipelines or on the local machine of a user.

---

## Prerequisites

There are a couple of things to be aware of before attempting to use the docker image (whether than be on a local machine or as a base image for another repository's pipeline):

If the pipeline for this repository has never been run, it will not have built and pushed a docker image to the GitLab Container Image Registry, therefore the docker image will be unavailable.

Furthermore, if changes have been made to any file in the `./docker` folder, the changes won't be reflected in the docker image within the GitLab Container Image Registry until this repository's pipeline has been successfully run/completed.

---

## Using the Pipeline-built Docker Image on a Local Machine

### Run the Docker Image Locally

1. Create a docker container from the docker image and start it running, as follows:

    ```bash
    docker run -it --rm registry.gitlab.com/seanburns/stm32-compiler-docker-image
    ```

---

## Using the Pipeline-built Docker Image as a Base Image for another Repository's Pipeline

To use the docker image that was created by this repository as a base image for another repository's pipeline, simply reference it at the top of the other repository's `.gitlab-ci.yml` file, as follows:

```yaml
image: registry.gitlab.com/seanburns/stm32-compiler-docker-image:latest
```

---

## Developing / Changing / Improving the Docker Image

When making changes to the docker image (i.e. changing the `./docker/Dockerfile` file), you might need to experiment with trial and error to see that the changes work as expected. In such a scenario it can be useful to build the docker image on your local computer and to run a local docker container using the image. Instructions below detail how to do this (but they assume that you have Docker installed on your computer).

### Building the Docker Image Locally

In the terminal of your computer (on which Docker is already installed):

```bash
cd <root-of-this-repository>

$GCC_COMPILER_NAME="arm-gnu-toolchain"
$GCC_COMPILER_VERSION="12.3.rel1"
$GCC_COMPILER_HOST_ARCHITECTURE="x86_64"
$GCC_COMPILER_TARGET="arm-none-eabi"

$GCC_COMPILER_BASIC_FILENAME="${GCC_COMPILER_NAME}-${GCC_COMPILER_VERSION}-${GCC_COMPILER_HOST_ARCHITECTURE}-${GCC_COMPILER_TARGET}"

$GCC_COMPILER_FILENAME="${GCC_COMPILER_BASIC_FILENAME}.tar.xz"
$GCC_COMPILER_URL="https://developer.arm.com/-/media/Files/downloads/gnu/${GCC_COMPILER_VERSION}/binrel/${GCC_COMPILER_FILENAME}"
$GCC_COMPILER_PATH="/${GCC_COMPILER_BASIC_FILENAME}/bin/"

$CI_REGISTRY_IMAGE="stm32-compiler-docker-image"
$TAG="${GCC_COMPILER_VERSION}-local"

docker build --build-arg GCC_COMPILER_FILENAME="${GCC_COMPILER_FILENAME}" --build-arg GCC_COMPILER_URL="${GCC_COMPILER_URL}" --build-arg GCC_COMPILER_PATH="${GCC_COMPILER_PATH}" --tag "${CI_REGISTRY_IMAGE}:${TAG}" ./docker
```

### Running a Container Using the Local Docker Image

In the terminal of your computer, start the container running:

```bash
cd <root-of-this-repository>
$GCC_COMPILER_VERSION="12.3.rel1"
docker run -it --rm stm32-compiler-docker-image:${GCC_COMPILER_VERSION}-local
```

Then, within the running container, try out running the compiler's executable:

```bash
arm-none-eabi-gcc --version
make --version
```

---

## Compiling an STM32 Project Locally Using the Docker Image

### Local Compilation Prerequisites

When following these instructions, in order to use the remote `stm32-compiler-docker-image` Docker Image from my Container Registry on GitLab (which is the preferred method), you will need to follow the instruction in the section, **Using the Pipeline-built Docker Image on a Local Machine**, above.

### Instructions

The following instructions (compatible with both Windows Powershell and Linux) show how you might build an STM32 project on your local computer using the Docker image sourced either from the GitLab Container Registry or from a locally built Docker image. The instructions assume that you have the Docker Engine already installed.

This can be useful for building the STM32 project in exactly the same way that the GitLab pipeline will build it.

1. Locate/obtain the STM32 project which you would like to build, on your local computer.

1. Either open a terminal on Linux or run Powershell as Administrator on Windows.

1. Change the current working directory to the path of the folder which contains the STM32 project (normally this would be the root of the Git repository containing the STM32 project):

    ```bash
    cd <root-of-repository-containing-stm32-project>
    ```

1. Start a Docker Container running with the `stm32-compiler-docker-image` Docker Image, mapping the current working directory of the host to the Docker Container's `/repository` path and attach to its terminal, using either:

    a. The remote `stm32-compiler-docker-image` Docker Image from my Container Registry on GitLab:

    ```bash
    $REMOTE_IMAGE="registry.gitlab.com/seanburns/stm32-compiler-docker-image:latest"
    docker pull ${REMOTE_IMAGE}
    docker run -it --rm -v ${PWD}:/repository ${REMOTE_IMAGE}
    ```

    b. Your local `stm32-compiler-docker-image` Docker Image, if you have previously built a local copy (see instructions above):

    ```bash
    docker run -it --rm -v ${PWD}:/repository stm32-compiler-docker-image:latest
    ```

1. Now that you are attached to the Docker Container's terminal, execute commands to build the project inside the Docker Container and then exit back to the host:

    ```bash
    # Change the current working directory (within the Docker machine) to the path which maps back to the repository (current working directory) of the Docker-machine's host:
    cd /repository

    # Execute commands such as make.

    exit
    ```
